﻿using System;
using System.Drawing;
using System.IO;
using System.Threading.Tasks;
using Foundation;
using UIKit;
using Wedly.iOS.DependencyServices;
using Wedly.Utilities.DependencyServices;
using Xamarin.Forms;

[assembly: Dependency(typeof(ImagePickerService))]
namespace Wedly.iOS.DependencyServices
{
    public class ImagePickerService : iImagePicker
    {
        TaskCompletionSource<Stream> taskCompletionSource;
        TaskCompletionSource<string> stringTaskCompletionSource;
        UIImagePickerController imagePickerController;

        public Task<Stream> GetImageFromGalleryAsync()
        {
            imagePickerController = new UIImagePickerController
            {
                SourceType = UIImagePickerControllerSourceType.PhotoLibrary,
                MediaTypes = UIImagePickerController.AvailableMediaTypes(UIImagePickerControllerSourceType.PhotoLibrary)

            };

            imagePickerController.FinishedPickingMedia += OnImagePickerFinishedPickingMedia;
            imagePickerController.Canceled += OnImagePickerCancelled;

            UIWindow uIWindow = UIApplication.SharedApplication.KeyWindow;
            var viewController = uIWindow.RootViewController;
            viewController.PresentModalViewController(imagePickerController, true);

            taskCompletionSource = new TaskCompletionSource<Stream>();
            return taskCompletionSource.Task;
        }
        void OnImagePickerFinishedPickingMedia(object sender, UIImagePickerMediaPickedEventArgs eventArgs)
        {
            UIImage uIImage = eventArgs.EditedImage ?? eventArgs.OriginalImage;

            if (uIImage != null)
            {
                NSData data = uIImage.AsJPEG(1);
                Stream stream = uIImage.AsJPEG(1).AsStream();

                var height = uIImage.CGImage.Height;
                var width = uIImage.CGImage.Width;
                var editedImage = ResizeImage(StreamtoByte(stream), 150, 120);

                taskCompletionSource.SetResult(new MemoryStream(editedImage));
            }
            else
            {
                taskCompletionSource.SetResult(null);
            }
            imagePickerController.DismissModalViewController(true);

        }
        void OnImagePickerCancelled(object sender, EventArgs eventArgs)
        {
            taskCompletionSource.SetResult(null);
            imagePickerController.DismissModalViewController(true);

        }
        public byte[] ResizeImage(byte[] imageData, float width, float height)
        {
            UIImage originalImage = ImageFromByteArray(imageData);

            UIGraphics.BeginImageContext(new SizeF(width, height));
            originalImage.Draw(new System.Drawing.Rectangle(0, 0, (int)width, (int)height));

            var resizedImage = UIGraphics.GetImageFromCurrentImageContext();
            UIGraphics.EndImageContext();

            var bytesImagen = resizedImage.AsJPEG().ToArray();
            resizedImage.Dispose();
            return bytesImagen;

        }
        public UIImage ImageFromByteArray(byte[] data)
        {
            if (data == null)
                return null;

            return new UIKit.UIImage(Foundation.NSData.FromArray(data));

        }
        public static byte[] StreamtoByte(Stream input)
        {
            byte[] buffer = new byte[16 * 1024];
            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }

        public Task<string> GetImagePathAsync()
        {
            // Create and define UIImagePickerController
            imagePickerController = new UIImagePickerController
            {
                SourceType = UIImagePickerControllerSourceType.PhotoLibrary,
                MediaTypes = UIImagePickerController.AvailableMediaTypes(UIImagePickerControllerSourceType.PhotoLibrary)
            };

            // Set event handlers
            imagePickerController.FinishedPickingMedia += OnImagePickerFinished;
            imagePickerController.Canceled += OnImagePickerCancelled;

            // Present UIImagePickerController;
            var window = UIApplication.SharedApplication.KeyWindow;
            var vc = window.RootViewController;
            while (vc.PresentedViewController != null)
            {
                vc = vc.PresentedViewController;
            }
            vc.PresentModalViewController(imagePickerController, true);

            // Return Task object
            stringTaskCompletionSource = new TaskCompletionSource<string>();
            return stringTaskCompletionSource.Task;
        }
        private void OnImagePickerFinished(object sender, UIImagePickerMediaPickedEventArgs args)
        {
            UIImage image = args.EditedImage ?? args.OriginalImage;

            if (image != null)
            {

                var url = (NSUrl)args.Info.ValueForKey(new NSString("UIImagePickerControllerImageURL"));


                stringTaskCompletionSource.SetResult(url.Path);
            }
            else
            {
                taskCompletionSource.SetResult(null);
            }
            imagePickerController.DismissModalViewController(true);
        }
    }
}
