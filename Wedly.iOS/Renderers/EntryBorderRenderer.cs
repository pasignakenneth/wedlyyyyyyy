﻿using System;
using UIKit;
using Wedly.iOS.Renderers;
using Wedly.Utilities.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(EntryBorderRender), typeof(EntryBorderRenderer))]
namespace Wedly.iOS.Renderers
{
    public class EntryBorderRenderer : EntryRenderer
    {
        float animatedDistance;
        EntryBorderRender customEntry;

        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);
            if (e.NewElement != null)
            {
                customEntry = (EntryBorderRender)Element;
                Control.Layer.BorderWidth = 0;
                Control.BorderStyle = UIKit.UITextBorderStyle.None;
                Control.AutocapitalizationType = UIKit.UITextAutocapitalizationType.None;

                Control.EditingDidBegin += OnEntryDidEdit;
                Control.EditingDidEnd += OnEntryDidEnd;
            }
        }
        private void OnEntryDidEnd(object sender, EventArgs e)
        {
            UIView parentView = getParentView();
            var viewFrame = parentView.Bounds;

            viewFrame.Y = 0.0f;

            UIView.BeginAnimations(null, (IntPtr)null);
            UIView.SetAnimationBeginsFromCurrentState(true);
            UIView.SetAnimationDuration(0.3);

            parentView.Frame = viewFrame;

            UIView.CommitAnimations();
        }

        private void OnEntryDidEdit(object sender, EventArgs e)
        {
            UIView parentWindow = getParentView();
            var textfieldRect = parentWindow.ConvertRectFromView(Control.Bounds, Control);
            var viewRect = parentWindow.ConvertRectFromView(parentWindow.Bounds, parentWindow);

            float midline = (float)(textfieldRect.Y + 0.5 * textfieldRect.Height);
            float numerator = (float)(midline - viewRect.Y - 0.2 * viewRect.Height);
            float denominator = (float)((1.0f - 0.2f) * viewRect.Height);
            float heightFraction = numerator / denominator;

            if (heightFraction < 0.0)
            {
                heightFraction = 0.0f;
            }
            else if (heightFraction > 1.0)
            {
                heightFraction = 1.0f;
            }

            UIInterfaceOrientation orientation = UIApplication.SharedApplication.StatusBarOrientation;
            if (orientation == UIInterfaceOrientation.Portrait || orientation == UIInterfaceOrientation.PortraitUpsideDown)
            {
                if (customEntry.Keyboard == Keyboard.Numeric || customEntry.Keyboard == Keyboard.Telephone)
                {
                    animatedDistance = (float)Math.Floor((216.0f + 44.0f) * heightFraction);
                }
                else
                {
                    animatedDistance = (float)Math.Floor(216.0f * heightFraction);
                }
            }
            else
            {
                if (customEntry.Keyboard == Keyboard.Numeric || customEntry.Keyboard == Keyboard.Telephone)
                {
                    animatedDistance = (float)Math.Floor((162.0f + 44.0f) * heightFraction);
                }
                else
                {
                    animatedDistance = (float)Math.Floor(162.0f * heightFraction);
                }
            }

            var viewFrame = parentWindow.Frame;
            viewFrame.Y -= animatedDistance;

            UIView.BeginAnimations(null, (IntPtr)null);
            UIView.SetAnimationBeginsFromCurrentState(true);
            UIView.SetAnimationDuration(0.3);

            parentWindow.Frame = viewFrame;

            UIView.CommitAnimations();
        }

        UIView getParentView()
        {
            UIView view = Control.Superview;

            while (view != null && !(view is UIWindow))
            {
                view = view.Superview;
            }

            return view;
        }

    }
}
