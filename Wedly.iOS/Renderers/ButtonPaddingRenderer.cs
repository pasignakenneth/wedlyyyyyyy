﻿using System;
using Wedly.iOS.Renderers;
using Wedly.Utilities.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(ButtonPaddingRender), typeof(ButtonPaddingRenderer))]
namespace Wedly.iOS.Renderers
{
    public class ButtonPaddingRenderer : ButtonRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Button> e)
        {
            base.OnElementChanged(e);

            if (Control != null)
            {
                Control.ContentEdgeInsets = new UIKit.UIEdgeInsets(0, 0, 0, 0);
            }
        }
    }
}
