﻿using System;
using Wedly.iOS.Renderers;
using Wedly.Utilities.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(DatePickerCustomRender), typeof(DatePickerCustomRenderer))]
namespace Wedly.iOS.Renderers
{
    public class DatePickerCustomRenderer : DatePickerRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<DatePicker> e)
        {
            base.OnElementChanged(e);

            Control.Layer.BorderWidth = 0;
            Control.BorderStyle = UIKit.UITextBorderStyle.None;
        }
    }
}
