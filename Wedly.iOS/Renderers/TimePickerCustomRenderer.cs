﻿using System;
using Wedly.iOS.Renderers;
using Wedly.Utilities.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(TimePickerCustomRender), typeof(TimePickerCustomRenderer))]
namespace Wedly.iOS.Renderers
{
    public class TimePickerCustomRenderer : TimePickerRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<TimePicker> e)
        {
            base.OnElementChanged(e);

            Control.Layer.BorderWidth = 0;
            Control.BorderStyle = UIKit.UITextBorderStyle.None;
        }
    }
}
