﻿
using System;
using System.Collections.Generic;
using Wedly.Models;
using Wedly.Utilities;
using Xamarin.Forms;
using Wedly.Utilities.Helpers.WebServiceReader;
using Newtonsoft.Json.Linq;
using System.Threading;
using Newtonsoft.Json;

namespace Wedly.Views
{
    public partial class Signup : RootViewPage, iWebServiceConnector
    {
        WebServiceReader webServiceReader = new WebServiceReader();
        CancellationToken ct = new CancellationToken();

        public Signup()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            this.LeftIcon = "BackButton";
            this.LeftIcon_Command = new Command(() => Navigation.PopAsync());

            webServiceReader.WebServiceReaderDelegate = this;
        }

        async void Signup_Clicked()
        {
            //not this
            //if(!String.IsNullOrEmpty(UsernameEntry.Text) && !String.IsNullOrEmpty(NameEntry.Text) && !String.IsNullOrEmpty(PasswordEntry.Text))
            //{
            //    CONSTANTS.getUsers.Add(new Users() { email = UsernameEntry.Text, name = NameEntry.Text, token = PasswordEntry.Text });
            //    Application.Current.Properties.Add("user_active", UsernameEntry.Text);
            //    await Application.Current.SavePropertiesAsync();
            //    App.Current.MainPage = new NavigationPage(new DashboardPage());
            //}

            //implement this
            if (String.IsNullOrEmpty(NameEntry.Text) || String.IsNullOrEmpty(UsernameEntry.Text) || String.IsNullOrEmpty(PasswordEntry.Text))
            {
                UsernameEntry.PlaceholderColor = Color.Red;
                NameEntry.PlaceholderColor = Color.Red;
                PasswordEntry.PlaceholderColor = Color.Red;
            }
            else
            {
                await webServiceReader.CreateData(CONSTANTS.URL + CONSTANTS.URL_VERSION + CONSTANTS.URL_REGISTER, new { user = new { email = UsernameEntry.Text, name = NameEntry.Text, password = PasswordEntry.Text }, device = new { token = "devicetoken123456", os = 1} }, ct);
            }
        }
        void Login_Clicked()
        {
            Navigation.PopAsync();
        }
        public void ReceiveJSONDataAsync(JObject jsonData, CancellationToken ct)
        {
            System.Diagnostics.Debug.WriteLine(jsonData);
            if(int.Parse(jsonData["status"].ToString()) == 200)
            {
                CONSTANTS.ShortMessage("Registered. Logging in..");
                var currentUser = JsonConvert.DeserializeObject<Users>(jsonData["user"].ToString());
                CONSTANTS.getCurrentUser = currentUser;
                Application.Current.Properties.Add("user_active", CONSTANTS.getCurrentUser.token);
                Application.Current.SavePropertiesAsync();
                Application.Current.MainPage = new NavigationPage(new DashboardPage());
            }
            else if(int.Parse(jsonData["status"].ToString()) == 300)
            {
                ErrorLabel.IsVisible = true;
                ErrorLabel.Text = jsonData["error"].ToString();
            }
        }

        public void ReceiveTimeoutError(int error, string errorStr)
        {
            throw new NotImplementedException();
        }

    }
}
