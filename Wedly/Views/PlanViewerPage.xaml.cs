﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Threading;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Plugin.Media;
using Plugin.Media.Abstractions;
using Wedly.Models;
using Wedly.Utilities;
using Wedly.Utilities.DependencyServices;
using Wedly.Utilities.Helpers.FileHelper;
using Xamarin.Forms;

namespace Wedly.Views
{
    public partial class PlanViewerPage : RootViewPage, iFileConnector
    {
        FileReader fileReader = FileReader.GetInstance;
        CancellationToken ct = new CancellationToken();
        int ctr = 1;
        public event EventHandler DialogClosed;
        public event EventHandler DialogShow;
        public event EventHandler DialogClosing;
        public event EventHandler DialogShowing;
        public static readonly BindableProperty HeaderTitleProperty = BindableProperty.Create("HeaderTitle", typeof(string), typeof(PlanViewerPage), string.Empty, BindingMode.TwoWay);
        public static readonly BindableProperty CloseButtonProperty = BindableProperty.Create("CloseButtonClicked", typeof(bool), typeof(PlanViewerPage), false, BindingMode.TwoWay);

        public PlanViewerPage()
        {
            InitializeComponent();
            fileReader.FileReaderDelegate = this;
            PopUpBgLayout.GestureRecognizers.Add(new TapGestureRecognizer
            {
                Command = new Command(HideDialog)
            });

            PopUpDialogClose.GestureRecognizers.Add(new TapGestureRecognizer
            {
                Command = new Command(HideDialog)
            });
        }
        public string HeaderTitle
        {
            get { return (string)GetValue(HeaderTitleProperty); }
            set { SetValue(HeaderTitleProperty, value); }
        }
        public bool CloseButtonClicked
        {
            get { return (bool)GetValue(CloseButtonProperty); }
            set { SetValue(CloseButtonProperty, value); }
        }

        void Logout_Clicked()
        {
            
        }
        async void OpenGallery_Clicked(object sender, EventArgs eventArgs)
        {
            Stream stream = await DependencyService.Get<iImagePicker>().GetImageFromGalleryAsync();

            if (stream != null)
                galleryImage.Source = ImageSource.FromStream(() => stream);

        }
        async void TakePhoto_Clicked(object sender, EventArgs eventArgs)
        {
            if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
                return;

            var file = await CrossMedia.Current.TakePhotoAsync(new StoreCameraMediaOptions
            {
                Directory = "Test",
                SaveToAlbum = true,
                CompressionQuality = 100,
                CustomPhotoSize = 100,
                PhotoSize = PhotoSize.MaxWidthHeight,
                MaxWidthHeight = 1080,
                DefaultCamera = CameraDevice.Front
            });

            if (file == null)
                return;

            //galleryImage.Source = ImageSource.FromStream(() =>
            //{
            //    var stream = file.GetStream();
            //    file.Dispose();
            //    return stream;
            //});

        }
        void Plan_ItemTapped(object sender, ItemTappedEventArgs eventArgs)
        {
            var data = (Plans)listview.SelectedItem;
            ShowDialog(data);
            listview.SelectedItem = null;
        }
        void Plan_ItemAppearing(object sender, ItemVisibilityEventArgs eventArgs)
        {
            //var index = CONSTANTS.getPlans.IndexOf((Plans)eventArgs.Item);

            //if(index == (CONSTANTS.getPagination.off_set-1))
            //{
            //    await webServiceReader.RetrieveData(Constants.URL + Constants.pagination.url, ct); 
            //}

        }
        public void ReceiveJSONDataAsync(JObject jsonData, CancellationToken ct)
        {
            System.Diagnostics.Debug.WriteLine(jsonData);
            var data = JsonConvert.DeserializeObject<ObservableCollection<Plans>>(jsonData["plans"].ToString());
            listview.ItemsSource = data;

            //if(ctr==1)
            //{
            //    var data_pagination = JsonConvert.DeserializeObject<Pagination>(jsonData["pagination"].ToString());
            //    CONSTANTS.getPagination = data_pagination;
            //    var datas = JsonConvert.DeserializeObject<ObservableCollection<Plans>>(jsonData["plans"].ToString());
            //    foreach (var x in datas)
            //    {
            //        CONSTANTS.getPlans.Add(x);
            //    }

            //    ctr = CONSTANTS.getPagination.load_more;
            //    listview.ItemsSource = CONSTANTS.getPlans;
            //}
        }

        public void ReceiveTimeoutError(int error, string errorStr)
        {
            throw new NotImplementedException();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            Device.BeginInvokeOnMainThread(async () =>
            {
                await fileReader.ReadFile("WeddingPlansJSONFile.json", ct);
            });
        }

        public void ShowDialog(Plans plans)
        {
            BindingContext = plans;
            ShowDialogAnimation(PopUpDialogLayout, PopUpBgLayout);
            CloseButtonClicked = false;
        }

        public void HideDialog()
        {
            HideDialogAnimation(PopUpDialogLayout, PopUpBgLayout);
            CloseButtonClicked = true;
        }

        public View DialogContent
        {
            get { return ContentView.Content; }
            set { ContentView.Content = value; }
        }

        protected virtual void OnDialogClosed(EventArgs e)
        {
            DialogClosed?.Invoke(this, e);
        }

        protected virtual void OnDialogShow(EventArgs e)
        {
            DialogShow?.Invoke(this, e);
        }

        protected virtual void OnDialogClosing(EventArgs e)
        {
            DialogClosing?.Invoke(this, e);
        }

        protected virtual void OnDialogShowing(EventArgs e)
        {
            DialogShowing?.Invoke(this, e);
        }

        private void ShowDialogAnimation(VisualElement dialog, VisualElement bg)
        {
            dialog.TranslationY = bg.Height;
            bg.IsVisible = true;
            dialog.IsVisible = true;

            ////ANIMATIONS 
            var showBgAnimation = OpacityAnimation(bg, 0, 0.5);
            var showDialogAnimation = TransLateYAnimation(dialog, bg.Height, 0);

            ////EXECUTE ANIMATIONS
            this.Animate("showBg", showBgAnimation, 16, 200, Easing.Linear, (d, f) => { });
            this.Animate("showMenu", showDialogAnimation, 16, 200, Easing.Linear, (d, f) =>
            {
                OnDialogShow(new EventArgs());
            });

            OnDialogShowing(new EventArgs());
        }

        private void HideDialogAnimation(VisualElement dialog, VisualElement bg)
        {
            //ANIMATIONS     
            var hideBgAnimation = OpacityAnimation(bg, 0.5, 0);
            var showDialogAnimation = TransLateYAnimation(dialog, 0, bg.Height);

            ////EXECUTE ANIMATIONS
            this.Animate("hideBg", hideBgAnimation, 16, 200, Easing.Linear, (d, f) => { });
            this.Animate("hideMenu", showDialogAnimation, 16, 200, Easing.Linear, (d, f) =>
            {
                bg.IsVisible = false;
                dialog.IsVisible = false;
                dialog.TranslationY = PopUpBgLayout.Height;

                OnDialogClosed(new EventArgs());
            });

            OnDialogClosing(new EventArgs());
        }

        private static Animation TransLateYAnimation(VisualElement element, double from, double to)
        {
            return new Animation(d => { element.TranslationY = d; }, from, to);
        }

        private static Animation TransLateXAnimation(VisualElement element, double from, double to)
        {
            return new Animation(d => { element.TranslationX = d; }, from, to);
        }

        private static Animation OpacityAnimation(VisualElement element, double from, double to)
        {
            return new Animation(d => { element.Opacity = d; }, from, to);
        }


    }
}
