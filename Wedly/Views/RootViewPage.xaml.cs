﻿using System;
using System.Collections.Generic;
using System.Windows.Input;
using Xamarin.Forms;

namespace Wedly
{
    public partial class RootViewPage : ContentPage
    {
        public static readonly BindableProperty LeftIconProperty = BindableProperty.Create("LeftIcon",typeof(string), typeof(RootViewPage), null );
        public static readonly BindableProperty LeftIconCommandProperty = BindableProperty.Create("LeftIcon_Command", typeof(ICommand), typeof(RootViewPage), null);
        public static readonly BindableProperty CenterTextProperty = BindableProperty.Create("CenterText", typeof(string), typeof(RootViewPage), null);
        public static readonly BindableProperty CenterIconProperty = BindableProperty.Create("CenterIcon", typeof(string), typeof(RootViewPage), null);
        public static readonly BindableProperty CenterIconCommandProperty = BindableProperty.Create("CenterIcon_Command", typeof(ICommand), typeof(RootViewPage), null);
        public static readonly BindableProperty RightIconProperty = BindableProperty.Create("RightIcon", typeof(string), typeof(RootViewPage), null);
        public static readonly BindableProperty RightIconCommandProperty = BindableProperty.Create("RightIcon_Command", typeof(ICommand), typeof(RootViewPage), null);
        public RootViewPage()
        {
            InitializeComponent();
        }
        public string LeftIcon
        {
            get
            {
                return (string)GetValue(LeftIconProperty);
            }
            set
            {
                SetValue(LeftIconProperty, value);
            }
        }
        public ICommand LeftIcon_Command
        {
            get
            {
                return (ICommand)GetValue(LeftIconCommandProperty);
            }
            set
            {
                SetValue(LeftIconCommandProperty, value);
            }
        }
        public string CenterText
        {
            get
            {
                return (string)GetValue(CenterTextProperty);
            }
            set
            {
                SetValue(CenterTextProperty, value);
            }
        }
        public string CenterIcon
        {
            get
            {
                return (string)GetValue(CenterIconProperty);
            }
            set
            {
                SetValue(CenterIconProperty, value);
            }
        }
        public ICommand CenterIcon_Command
        {
            get
            {
                return (ICommand)GetValue(CenterIconCommandProperty);
            }
            set
            {
                SetValue(CenterIconCommandProperty, value);
            }
        }
        public string RightIcon
        {
            get
            {
                return (string)GetValue(RightIconProperty);
            }
            set
            {
                SetValue(RightIconProperty, value);
            }
        }
        public ICommand RightIcon_Command
        {
            get
            {
                return (ICommand)GetValue(RightIconCommandProperty);
            }
            set
            {
                SetValue(RightIconCommandProperty, value);
            }
        }
    }
}
