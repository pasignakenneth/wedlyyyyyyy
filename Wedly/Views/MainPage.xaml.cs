﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Wedly.Models;
using Wedly.Utilities;
using Wedly.Utilities.Helpers.WebServiceReader;
using Wedly.Views;
using Xamarin.Forms;

namespace Wedly
{
    public partial class MainPage : RootViewPage, iWebServiceConnector
    {
        WebServiceReader webServiceReader = new WebServiceReader();
        CancellationToken ct = new CancellationToken();
        public MainPage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);

            webServiceReader.WebServiceReaderDelegate = this;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            CONSTANTS.getAttendees.Clear();
            CONSTANTS.getPlansOwn.Clear();
            CONSTANTS.getPlansInvited.Clear();
            CONSTANTS.getUsers.Clear();
            CONSTANTS.getComments.Clear();
            CONSTANTS.getPagination = null;
            CONSTANTS.getCurrentUser = null;
            CONSTANTS.getInvitedUsers.Clear();
            CONSTANTS.getInvitedAttendees.Clear();
        }

        async void Login_Clicked(object sender, EventArgs eventArgs)
        {
            try
            {
                //not this
                //if(!String.IsNullOrEmpty(UsernameEntry.Text) && !String.IsNullOrEmpty(PasswordEntry.Text))
                //{
                //    Application.Current.Properties.Add("user_active", UsernameEntry.Text);
                //    await Application.Current.SavePropertiesAsync();
                //    Application.Current.MainPage = new NavigationPage (new DashboardPage());
                //}
                //else
                //{
                //    UsernameEntry.PlaceholderColor = Color.Red;
                //    PasswordEntry.PlaceholderColor = Color.Red;
                //}

                //implement this
                if (String.IsNullOrEmpty(UsernameEntry.Text) || String.IsNullOrEmpty(PasswordEntry.Text))
                {
                    UsernameEntry.PlaceholderColor = Color.Red;
                    PasswordEntry.PlaceholderColor = Color.Red;
                }
                else
                {
                    await webServiceReader.CreateData(CONSTANTS.URL + CONSTANTS.URL_VERSION + CONSTANTS.URL_SIGNIN, new { user = new { email = UsernameEntry.Text, password = PasswordEntry.Text.Trim() }, device = new { token = "devicetoken1234567", os = 1} }, ct);
                }

            }
            catch(Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.StackTrace);
            }

        }
        bool isValidEmail(string email)
        {
            try
            {
                var address = new System.Net.Mail.MailAddress(email);
                return address.Address == email;
            }
            catch
            {
                return false;
            }
            
        }

        void Register_Clicked()
        {
            Navigation.PushAsync(new Signup());
        }

        public void ReceiveJSONDataAsync(JObject jsonData, CancellationToken ct)
        {
            System.Diagnostics.Debug.WriteLine(jsonData);
            if(int.Parse(jsonData["status"].ToString()) == 200)
            {
                CONSTANTS.ShortMessage("Logging in..");
                var currentUser = JsonConvert.DeserializeObject<Users>(jsonData["session"].ToString());
                CONSTANTS.getCurrentUser = currentUser;
                Application.Current.Properties.Add("user_active", CONSTANTS.getCurrentUser.token);
                Application.Current.SavePropertiesAsync();
                Application.Current.MainPage = new NavigationPage(new DashboardPage());
            }
            else if(int.Parse(jsonData["status"].ToString()) == 300)
            {
                ErrorLabel.IsVisible = true;
                ErrorLabel.Text = jsonData["error"].ToString();
            }
        }

        public void ReceiveTimeoutError(int error, string errorStr)
        {
            throw new NotImplementedException();
        }
    }
}
