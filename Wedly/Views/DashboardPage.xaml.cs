﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Wedly.Models;
using Wedly.Utilities;
using Wedly.Utilities.Helpers.FileHelper;
using Wedly.Utilities.Helpers.WebServiceReader;
using Wedly.Utilities.Helpers.NetworkReader;
using Xamarin.Forms;

namespace Wedly.Views
{
    public partial class DashboardPage : RootViewPage, iFileConnector, iWebServiceConnector
    {
        FileReader fileReader = FileReader.GetInstance;
        WebServiceReader webServiceReader = new WebServiceReader();
        NetworkReader networkReader = NetworkReader.GetInstance;
        CancellationToken ct = new CancellationToken();
        Plans plans = new Plans();
        int ctr = 1;

        public DashboardPage()
        {
            InitializeComponent();
            fileReader.FileReaderDelegate = this;
            webServiceReader.WebServiceReaderDelegate = this;

            //Device.BeginInvokeOnMainThread(async () =>
            //{
            //    if (networkReader.HasInternet())
            //    {
            //        if (await networkReader.IsHostReachable())
            //        {
            //            await webServiceReader.RetrieveData(CONSTANTS.URL + CONSTANTS.URL_VERSION + CONSTANTS.URL_PLANS + CONSTANTS.URL_TEST_DASHBOARD + Application.Current.Properties["user_active"] , ct);
            //        }
            //    }
            //    else
            //    {
            //        await fileReader.ReadFile("WeddingPlansJSONFile.json", ct);
            //    }

            //});
            NavigationPage.SetHasNavigationBar(this, false);
            this.CenterIcon = "CreateButton";
            this.CenterIcon_Command = new Command(() => Navigation.PushAsync(new CreatePlanPage(plans, false)));
            this.RightIcon = "LogoutButton";
            this.RightIcon_Command = new Command(() => Logout_Clicked());
        }
        void Logout_Clicked()
        {
            Application.Current.Properties.Remove("user_active");
            Application.Current.SavePropertiesAsync();
            Application.Current.MainPage = new NavigationPage(new MainPage());

        }
        void OwnButton_Clicked(object sender, EventArgs eventArgs)
        {
            Button button = (Button)sender;

            button.BackgroundColor = Color.Purple;
            InvitedButton.BackgroundColor = Color.White;

            listviewOwn.ItemsSource = CONSTANTS.getPlansOwn;
            listviewOwn.ClassId = "0";
        }
        void InvitedButton_Clicked(object sender, EventArgs eventArgs)
        {
            Button button = (Button)sender;

            button.BackgroundColor = Color.Purple;
            OwnButton.BackgroundColor = Color.White;

            listviewOwn.ItemsSource = CONSTANTS.getPlansInvited;
            listviewOwn.ClassId = "1";
        }

        void Plan_ItemTapped(object sender, ItemTappedEventArgs eventArgs)
        {
            var data = (Plans)listviewOwn.SelectedItem;

            if (listviewOwn.ClassId == "0")
                Navigation.PushAsync(new SelectedPlanPage(data, true));
            else if(listviewOwn.ClassId == "1")
                Navigation.PushAsync(new SelectedPlanPage(data, false));

            listviewOwn.SelectedItem = null;

        }
        void Invited_ItemTapped(object sender, ItemTappedEventArgs eventArgs)
        {
            var data = (Plans)listviewInvited.SelectedItem;

            Navigation.PushAsync(new SelectedPlanPage(data, false));
            listviewInvited.SelectedItem = null;

        }
        protected override void OnAppearing()
        {
            base.OnAppearing();
            CONSTANTS.getUsers.Clear();
            Device.BeginInvokeOnMainThread(async () =>
            {
                if (networkReader.HasInternet())
                {
                    if (await networkReader.IsHostReachable())
                    {
                        await webServiceReader.RetrieveData(CONSTANTS.URL + CONSTANTS.URL_VERSION + CONSTANTS.URL_PLANS + CONSTANTS.URL_TEST_DASHBOARD + Application.Current.Properties["user_active"], ct);
                    }
                }
                else
                {
                    await fileReader.ReadFile("WeddingPlansJSONFile.json", ct);
                }

            });

        }
        async void ListViewOwn_ItemAppearing(object sender, ItemVisibilityEventArgs eventArgs)
        {
            //implement this
            var index = CONSTANTS.getPlansOwn.IndexOf((Plans)eventArgs.Item);

            if(index == (CONSTANTS.getPagination.off_set-1))
            {
                
                await webServiceReader.RetrieveData(CONSTANTS.ROOT_URL + CONSTANTS.getPagination.url, ct);
            }

        }

        async void ListViewInvited_ItemAppearing(object sender, ItemVisibilityEventArgs eventArgs)
        {
            var index = CONSTANTS.getPlansOwn.IndexOf((Plans)eventArgs.Item);

            if (index == (CONSTANTS.getPagination.off_set - 1))
            {
                await webServiceReader.RetrieveData(CONSTANTS.ROOT_URL + CONSTANTS.getPagination.url, ct);
            }
        }

        public void ReceiveJSONDataAsync(JObject jsonData, CancellationToken ct)
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
            try
            {
                    System.Diagnostics.Debug.WriteLine(jsonData);

                    if (networkReader.HasInternet())
                    {
                        if (await networkReader.IsHostReachable())
                        {
                            
                            if (int.Parse(jsonData["status"].ToString()) == 200)
                            {

                                if (ctr == 1)
                                {
                                    var pagination = JsonConvert.DeserializeObject<Pagination>(jsonData["pagination"].ToString());
                                    CONSTANTS.getPagination = pagination;

                                    var dataOwn = JsonConvert.DeserializeObject<ObservableCollection<Plans>>(jsonData["plans"].ToString());
                                    foreach (var x in dataOwn)
                                    {
                                        x.banner_photo = CONSTANTS.ROOT_URL + x.banner_photo;
                                        x.groom_photo = CONSTANTS.ROOT_URL + x.groom_photo;
                                        x.bride_photo = CONSTANTS.ROOT_URL + x.bride_photo;
                                        CONSTANTS.getPlansOwn.Add(x);
                                    }
                                    var dataInvited = JsonConvert.DeserializeObject<ObservableCollection<Plans>>(jsonData["invited"].ToString());
                                    foreach (var x in dataInvited)
                                    {
                                        CONSTANTS.getPlansInvited.Add(x);
                                    }

                                    ctr = CONSTANTS.getPagination.load_more;
                                    listviewOwn.ItemsSource = CONSTANTS.getPlansOwn;
                                    listviewInvited.ItemsSource = CONSTANTS.getPlansInvited;
                                }

                            }
                        }
                    }
                    else
                    {
                        if (CONSTANTS.getPlansOwn.Count == 0 && CONSTANTS.getPlansInvited.Count == 0)
                        {
                            System.Diagnostics.Debug.WriteLine(jsonData);
                            var dataUser = JsonConvert.DeserializeObject<Users>(jsonData["user"].ToString());
                            var dataOwn = JsonConvert.DeserializeObject<ObservableCollection<Plans>>(jsonData["own"].ToString());
                            var dataInvited = JsonConvert.DeserializeObject<ObservableCollection<Plans>>(jsonData["invited"].ToString());
                            CONSTANTS.getCurrentUser = dataUser;
                            foreach (var x in dataOwn)
                            {
                                CONSTANTS.getPlansOwn.Add(x);
                            }
                            foreach (var x in dataInvited)
                            {
                                CONSTANTS.getPlansInvited.Add(x);
                            }
                            listviewOwn.ItemsSource = CONSTANTS.getPlansOwn;
                            listviewInvited.ItemsSource = CONSTANTS.getPlansInvited;
                        }
                    }

                }
                catch (Exception ex)
                {
                    
                }
            });

        }

        public void ReceiveTimeoutError(int error, string errorStr)
        {
            //isDBCalled = true;
            //await databaseReader.FetchData<Events>(ct);
        }
    }
}
