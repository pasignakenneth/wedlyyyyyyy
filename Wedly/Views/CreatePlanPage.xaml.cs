﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows.Input;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Wedly.Models;
using Wedly.Utilities;
using Wedly.Utilities.DependencyServices;
using Wedly.Utilities.Helpers.FileHelper;
using Xamarin.Forms;
using Wedly.Utilities.Helpers.WebServiceReader;
using System.Threading.Tasks;

namespace Wedly.Views
{
    public partial class CreatePlanPage : RootViewPage, iFileConnector, iWebServiceConnector
    {
        FileReader fileReader = FileReader.GetInstance;
        WebServiceReader webService = new WebServiceReader();
        CancellationToken ct = new CancellationToken();
        public event EventHandler DialogClosed;
        public event EventHandler DialogShow;
        public event EventHandler DialogClosing;
        public event EventHandler DialogShowing;
        public static readonly BindableProperty HeaderTitleProperty = BindableProperty.Create("HeaderTitle", typeof(string), typeof(SelectedPlanPage), string.Empty, BindingMode.TwoWay);
        public static readonly BindableProperty CloseButtonProperty = BindableProperty.Create("CloseButtonClicked", typeof(bool), typeof(SelectedPlanPage), false, BindingMode.TwoWay);
        int ctrListview = 0;
        int cameHere = 0;
        bool gotUsers = false;
        Plans planned = new Plans();
        Stream[] stream = new MemoryStream[3];


        public CreatePlanPage(Plans plann, bool isEdit)
        {
            InitializeComponent();
            fileReader.FileReaderDelegate = this;
            webService.WebServiceReaderDelegate = this;
            PopUpBgLayout.GestureRecognizers.Add(new TapGestureRecognizer
            {
                Command = new Command(HideDialog)
            });

            PopUpDialogClose.GestureRecognizers.Add(new TapGestureRecognizer
            {
                Command = new Command(HideDialog)
            });


            if (isEdit)
            {
                System.Diagnostics.Debug.WriteLine(StartTimePicker.Time.ToString());
                BindingContext = plann;
                CreateButton.Text = "Edit";
                planned = plann;
                System.Diagnostics.Debug.WriteLine("sadasd: "+planned.attendees.Count);
                if (!gotUsers)
                {
                    
                    System.Diagnostics.Debug.WriteLine(CONSTANTS.getInvitedUsers.Count);
                    System.Diagnostics.Debug.WriteLine("isEdit: "+CONSTANTS.getInvitedAttendees.Count);
                    gotUsers = true;
                }
                StartDatePicker.TextColor = Color.White;
                StartTimePicker.TextColor = Color.White;
            }
            else
            {
                //CONSTANTS.getUsers.Clear();
                CONSTANTS.getInvitedUsers.Clear();
                CONSTANTS.getInvitedAttendees.Clear();
                CreateButton.Text = "Create";
                this.LeftIcon = "BackButton";
                this.LeftIcon_Command = new Command(() => Navigation.PopAsync());

            }
            NavigationPage.SetHasNavigationBar(this, false);
            this.RightIcon = "LogoutButton";
            this.RightIcon_Command = new Command(() => Logout_Clicked());

        }

        void Logout_Clicked()
        {
            Application.Current.Properties.Remove("user_active");
            Application.Current.SavePropertiesAsync();
            Application.Current.MainPage = new NavigationPage(new MainPage());

        }
        protected override void OnAppearing()
        {
            base.OnAppearing();
            Device.BeginInvokeOnMainThread(async() =>
            {
                await webService.RetrieveData(CONSTANTS.URL + CONSTANTS.URL_VERSION + CONSTANTS.URL_USERS + "?token=" + Application.Current.Properties["user_active"], ct);
            });
        }

        public async void UploadImage_Clicked(object sender, EventArgs eventArgs)
        {
            Button button = (Button)sender;

            Stream chosenImage = await DependencyService.Get<iImagePicker>().GetImageFromGalleryAsync();
            if(chosenImage != null)
            {
                using (MemoryStream ms = new MemoryStream())
                {
                    chosenImage.CopyTo(ms);
                    byte[] chosen = ms.ToArray();
                    if (button.StyleId == "0")
                    {
                        stream[0] = new MemoryStream(chosen);
                        WeddingBannerImage.Source = ImageSource.FromStream(() => new MemoryStream(chosen));
                    }
                    else if (button.StyleId == "1")
                    {
                        stream[1] = new MemoryStream(chosen);
                        GroomImage.Source = ImageSource.FromStream(() => new MemoryStream(chosen));
                    }
                    else if(button.StyleId == "2")
                    {
                        stream[2] = new MemoryStream(chosen);
                        BrideImage.Source = ImageSource.FromStream(() => new MemoryStream(chosen));
                    }
                }
            }
        }

        void AddAttendees_Clicked(object seder, EventArgs eventArgs)
        {
            listviewInviteUser.ClassId = "0";

            if (cameHere == 1)
            {
                System.Diagnostics.Debug.WriteLine("Add attendees");
                listviewInviteUser.ItemsSource = CONSTANTS.getUsers;
            }
            ShowDialog();
        }
        void ViewAttendees_Clicked()
        {
            listviewInviteUser.ClassId = "1";
            listviewInviteUser.ItemsSource = CONSTANTS.getInvitedUsers;
            ShowDialog();
        }
        void InviteUser_ItemTapped(object sender, ItemTappedEventArgs eventArgs)
        {
            listviewInviteUser.SelectedItem = null;
        }
        void InviteUser_Clicked(object sender, EventArgs eventArgs)
        {
            var item = (Button)sender;

            if (listviewInviteUser.ClassId.Equals("0"))
            {
                Users userAdd = (from itm in CONSTANTS.getUsers where itm.name == item.CommandParameter.ToString() select itm).FirstOrDefault<Users>();
                CONSTANTS.getInvitedUsers.Add(userAdd);
                CONSTANTS.getUsers.Remove(userAdd);

                System.Diagnostics.Debug.WriteLine("getInvitedAttendees count: " + CONSTANTS.getInvitedAttendees.Count);
                System.Diagnostics.Debug.WriteLine("getInvitedUsers count: " + CONSTANTS.getInvitedUsers.Count);
            }
            else if(listviewInviteUser.ClassId.Equals("1"))
            {
                Users userDelete = (from itm in CONSTANTS.getInvitedUsers where itm.name == item.CommandParameter.ToString() select itm).FirstOrDefault<Users>();
                CONSTANTS.getInvitedUsers.Remove(userDelete);
                CONSTANTS.getUsers.Add(userDelete);
            }
        }
        async void CreatePlan_Clicked()
        {
            
            foreach (var x in CONSTANTS.getInvitedUsers)
            {
                CONSTANTS.inviteUsers.Add(new InviteUsers { user_id = x.id, role = x.role });
            }

            JObject newPlan = new JObject();
            JObject jObject = new JObject();

            newPlan.Add("title", TitleEntry.Text);
            //implement this
            //newPlan.Add("officiator", OfficiatorEntry.Text);
            DateTime date = StartDatePicker.Date.Add(StartTimePicker.Time);
            newPlan.Add("start_at", date.ToString("dd/MM/yyyy hh:mm tt"));
            newPlan.Add("banner_photo", "");
            newPlan.Add("groom_photo", "");
            newPlan.Add("bride_photo", "");
            newPlan.Add("location", LocationEntry.Text);
            //implement this
            //newPlan.Add("reception", ReceptionLocationEntry.Text);

            jObject.Add("plan", newPlan);
            jObject.Add("invite", JToken.FromObject(CONSTANTS.getInvitedUsers));

            if(CreateButton.Text=="Create")
            {
                
                List<string> keys = new List<string>() { "plan", "invite" };
                string url = CONSTANTS.URL + CONSTANTS.URL_VERSION + CONSTANTS.URL_PLANS + "?token=" + Application.Current.Properties["user_active"] ;

                await webService.MultiPartDataContentAsync(url, keys, ct, jObject, stream, null);

            }
            else
            {
                await Navigation.PopToRootAsync();
            }



        }
        public void ReceiveJSONDataAsync(JObject jsonData, CancellationToken ct)
        {
            System.Diagnostics.Debug.WriteLine(jsonData);
            
            if (cameHere == 0)
            {
                if (int.Parse(jsonData["status"].ToString()) == 200)
                {
                    var data = JsonConvert.DeserializeObject<ObservableCollection<Users>>(jsonData["users"].ToString());
                    foreach (var x in data)
                    {
                        CONSTANTS.getUsers.Add(x);
                    }
                    listviewInviteUser.ItemsSource = CONSTANTS.getUsers;
                    cameHere = 1;
                }
            }
            else if(cameHere == 1)
            {
                if (int.Parse(jsonData["status"].ToString()) == 200)
                {
                    var dataPlan = JsonConvert.DeserializeObject<Plans>(jsonData["plan"].ToString());
                    dataPlan.banner_photo = CONSTANTS.ROOT_URL + dataPlan.banner_photo;
                    dataPlan.groom_photo = CONSTANTS.ROOT_URL + dataPlan.groom_photo;
                    dataPlan.bride_photo = CONSTANTS.ROOT_URL + dataPlan.bride_photo;
                    dataPlan.attendees_count = dataPlan.attendees.Count;
                    Navigation.PushAsync(new SelectedPlanPage(dataPlan, true));

                }

            }
        }

        public void ReceiveTimeoutError(int error, string errorStr)
        {
            
        }

        public string HeaderTitle
        {
            get { return (string)GetValue(HeaderTitleProperty); }
            set { SetValue(HeaderTitleProperty, value); }
        }
        public bool CloseButtonClicked
        {
            get { return (bool)GetValue(CloseButtonProperty); }
            set { SetValue(CloseButtonProperty, value); }
        }
        public void ShowDialog()
        {
            ShowDialogAnimation(PopUpDialogLayout, PopUpBgLayout);
            CloseButtonClicked = false;
        }

        public void HideDialog()
        {
            HideDialogAnimation(PopUpDialogLayout, PopUpBgLayout);
            CloseButtonClicked = true;
        }

        public View DialogContent
        {
            get { return ContentView.Content; }
            set { ContentView.Content = value; }
        }

        protected virtual void OnDialogClosed(EventArgs e)
        {
            DialogClosed?.Invoke(this, e);
        }

        protected virtual void OnDialogShow(EventArgs e)
        {
            DialogShow?.Invoke(this, e);
        }

        protected virtual void OnDialogClosing(EventArgs e)
        {
            DialogClosing?.Invoke(this, e);
        }

        protected virtual void OnDialogShowing(EventArgs e)
        {
            DialogShowing?.Invoke(this, e);
        }

        private void ShowDialogAnimation(VisualElement dialog, VisualElement bg)
        {
            dialog.TranslationY = bg.Height;
            bg.IsVisible = true;
            dialog.IsVisible = true;

            ////ANIMATIONS 
            var showBgAnimation = OpacityAnimation(bg, 0, 0.5);
            var showDialogAnimation = TransLateYAnimation(dialog, bg.Height, 0);

            ////EXECUTE ANIMATIONS
            this.Animate("showBg", showBgAnimation, 16, 200, Easing.Linear, (d, f) => { });
            this.Animate("showMenu", showDialogAnimation, 16, 200, Easing.Linear, (d, f) =>
            {
                OnDialogShow(new EventArgs());
            });

            OnDialogShowing(new EventArgs());
        }

        private void HideDialogAnimation(VisualElement dialog, VisualElement bg)
        {
            //ANIMATIONS     
            var hideBgAnimation = OpacityAnimation(bg, 0.5, 0);
            var showDialogAnimation = TransLateYAnimation(dialog, 0, bg.Height);

            ////EXECUTE ANIMATIONS
            this.Animate("hideBg", hideBgAnimation, 16, 200, Easing.Linear, (d, f) => { });
            this.Animate("hideMenu", showDialogAnimation, 16, 200, Easing.Linear, (d, f) =>
            {
                bg.IsVisible = false;
                dialog.IsVisible = false;
                dialog.TranslationY = PopUpBgLayout.Height;

                OnDialogClosed(new EventArgs());
            });

            OnDialogClosing(new EventArgs());
        }

        private static Animation TransLateYAnimation(VisualElement element, double from, double to)
        {
            return new Animation(d => { element.TranslationY = d; }, from, to);
        }

        private static Animation TransLateXAnimation(VisualElement element, double from, double to)
        {
            return new Animation(d => { element.TranslationX = d; }, from, to);
        }

        private static Animation OpacityAnimation(VisualElement element, double from, double to)
        {
            return new Animation(d => { element.Opacity = d; }, from, to);
        }
    }
}
