﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Threading;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Plugin.Media;
using Plugin.Media.Abstractions;
using Wedly.Models;
using Wedly.Utilities;
using Wedly.Utilities.DependencyServices;
using Wedly.Utilities.Helpers.FileHelper;
using Xamarin.Forms;
using Wedly.Utilities.Helpers.WebServiceReader;

namespace Wedly.Views
{
    public partial class SelectedPlanPage : RootViewPage, iFileConnector, iWebServiceConnector
    {
        Plans planSelected = new Plans();
        FileReader fileReader = FileReader.GetInstance;
        WebServiceReader webService = new WebServiceReader();
        CancellationToken ct = new CancellationToken();
        int try1 = 0;
        int ctr = 1;
        public event EventHandler DialogClosed;
        public event EventHandler DialogShow;
        public event EventHandler DialogClosing;
        public event EventHandler DialogShowing;
        public static readonly BindableProperty HeaderTitleProperty = BindableProperty.Create("HeaderTitle", typeof(string), typeof(SelectedPlanPage), string.Empty, BindingMode.TwoWay);
        public static readonly BindableProperty CloseButtonProperty = BindableProperty.Create("CloseButtonClicked", typeof(bool), typeof(SelectedPlanPage), false, BindingMode.TwoWay);

        public SelectedPlanPage(Plans plan, bool isOwned)
        {
            InitializeComponent();
            BindingContext = plan;

            listviewComments.ItemsSource = CONSTANTS.getComments;
            fileReader.FileReaderDelegate = this;
            webService.WebServiceReaderDelegate = this;
            PopUpBgLayout.GestureRecognizers.Add(new TapGestureRecognizer
            {
                Command = new Command(HideDialog)
            });

            PopUpDialogClose.GestureRecognizers.Add(new TapGestureRecognizer
            {
                Command = new Command(HideDialog)
            });
            planSelected = plan;
            foreach(var x in planSelected.attendees)
            {
                try1++;
                System.Diagnostics.Debug.WriteLine("try: "+try1);

            }
            if (isOwned)
            {
                this.CenterIcon = "EditButton";
                this.CenterIcon_Command = new Command(() => push(planSelected));

            }

            if (plan.comments != null)
            {
                foreach (var x in plan.comments)
                {
                    CONSTANTS.getComments.Add(x);
                }

            }
            NavigationPage.SetHasNavigationBar(this, false);
            this.LeftIcon = "BackButton";
            this.LeftIcon_Command = new Command(() => pop());
            this.RightIcon = "LogoutButton";
            this.RightIcon_Command = new Command(() => Logout_Clicked());

        }
        void Logout_Clicked()
        {
            Application.Current.Properties.Remove("user_active");
            Application.Current.SavePropertiesAsync();
            Application.Current.MainPage = new NavigationPage(new MainPage());

        }
        void pop()
        {
            CONSTANTS.getAttendees.Clear();
            CONSTANTS.getComments.Clear();
            Device.BeginInvokeOnMainThread(async () =>
            {
                //implement this
                await webService.RetrieveData(CONSTANTS.URL + CONSTANTS.URL_VERSION + CONSTANTS.URL_PLANS + CONSTANTS.URL_TEST_DASHBOARD + Application.Current.Properties["user_active"], ct);
            });
            Navigation.PopToRootAsync();
        }
        void push(Plans plane)
        {
            CONSTANTS.getUsers.Clear();
            CONSTANTS.getInvitedUsers.Clear();
            CONSTANTS.getInvitedAttendees.Clear();
            System.Diagnostics.Debug.WriteLine("PUSH "+plane.attendees.Count);
            Navigation.PushAsync(new CreatePlanPage(plane, true));
        }
        protected override void OnAppearing()
        {
            base.OnAppearing();
            BindingContext = planSelected;

        }
        public string HeaderTitle
        {
            get { return (string)GetValue(HeaderTitleProperty); }
            set { SetValue(HeaderTitleProperty, value); }
        }
        public bool CloseButtonClicked
        {
            get { return (bool)GetValue(CloseButtonProperty); }
            set { SetValue(CloseButtonProperty, value); }
        }
        void RSVP_Clicked(object sender, EventArgs eventArgs)
        {
            var btn = (Label)sender;

            if (planSelected.rsvp == 0)
            {
                planSelected.rsvp = 1;
                btn.Text = "Attending";
            }
            else if(planSelected.rsvp == 1)
            {
                planSelected.rsvp = 2;
                btn.Text = "Not Decided";
            }
            else if(planSelected.rsvp == 2)
            {
                planSelected.rsvp = 0;
                btn.Text = "Not Attending";
            }
        }
        void Attendance_Clicked()
        {
            ShowDialog(planSelected);
        }
        async void CommentImage_Clicked()
        {
            Stream imageUri = await DependencyService.Get<iImagePicker>().GetImageFromGalleryAsync();
            if (imageUri != null)
            {
                CONSTANTS.getComments.Insert(0, new Comments { image_comment = ""+imageUri, name = CONSTANTS.getCurrentUser.name, profile_image = CONSTANTS.getCurrentUser.image});
            }
        }
        void AddComment_Clicked()
        {
            if (!String.IsNullOrEmpty(CommentEntry.Text))
            {
                CONSTANTS.getComments.Insert(0, new Comments { comment = CommentEntry.Text, name = CONSTANTS.getCurrentUser.name, profile_image = CONSTANTS.getCurrentUser.image });
                CommentEntry.Text = "";
            }
        }
        public void ReceiveJSONDataAsync(JObject jsonData, CancellationToken ct)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                try
                {
                    if (CONSTANTS.getPlansOwn.Count == 0 && CONSTANTS.getPlansInvited.Count == 0)
                    {
                        System.Diagnostics.Debug.WriteLine(jsonData);
                        var dataOwn = JsonConvert.DeserializeObject<ObservableCollection<Plans>>(jsonData["own"].ToString());
                        var dataInvited = JsonConvert.DeserializeObject<ObservableCollection<Plans>>(jsonData["invited"].ToString());
                        foreach (var x in dataOwn)
                        {
                            CONSTANTS.getPlansOwn.Add(x);
                        }
                        foreach (var x in dataInvited)
                        {
                            CONSTANTS.getPlansInvited.Add(x);
                        }
                    }

                }
                catch (Exception ex)
                {

                }
            });
        }

        public void ReceiveTimeoutError(int error, string errorStr)
        {
            throw new NotImplementedException();
        }

        public void ShowDialog(Plans plans)
        {
            
            System.Diagnostics.Debug.WriteLine("attendees count: "+plans.attendees.Count);
            listviewAttendance.ItemsSource = plans.attendees;
            ShowDialogAnimation(PopUpDialogLayout, PopUpBgLayout);
            CloseButtonClicked = false;
        }

        public void HideDialog()
        {
            HideDialogAnimation(PopUpDialogLayout, PopUpBgLayout);
            CloseButtonClicked = true;
        }

        public View DialogContent
        {
            get { return ContentView.Content; }
            set { ContentView.Content = value; }
        }

        protected virtual void OnDialogClosed(EventArgs e)
        {
            DialogClosed?.Invoke(this, e);
        }

        protected virtual void OnDialogShow(EventArgs e)
        {
            DialogShow?.Invoke(this, e);
        }

        protected virtual void OnDialogClosing(EventArgs e)
        {
            DialogClosing?.Invoke(this, e);
        }

        protected virtual void OnDialogShowing(EventArgs e)
        {
            DialogShowing?.Invoke(this, e);
        }

        private void ShowDialogAnimation(VisualElement dialog, VisualElement bg)
        {
            dialog.TranslationY = bg.Height;
            bg.IsVisible = true;
            dialog.IsVisible = true;

            ////ANIMATIONS 
            var showBgAnimation = OpacityAnimation(bg, 0, 0.5);
            var showDialogAnimation = TransLateYAnimation(dialog, bg.Height, 0);

            ////EXECUTE ANIMATIONS
            this.Animate("showBg", showBgAnimation, 16, 200, Easing.Linear, (d, f) => { });
            this.Animate("showMenu", showDialogAnimation, 16, 200, Easing.Linear, (d, f) =>
            {
                OnDialogShow(new EventArgs());
            });

            OnDialogShowing(new EventArgs());
        }

        private void HideDialogAnimation(VisualElement dialog, VisualElement bg)
        {
            //ANIMATIONS     
            var hideBgAnimation = OpacityAnimation(bg, 0.5, 0);
            var showDialogAnimation = TransLateYAnimation(dialog, 0, bg.Height);

            ////EXECUTE ANIMATIONS
            this.Animate("hideBg", hideBgAnimation, 16, 200, Easing.Linear, (d, f) => { });
            this.Animate("hideMenu", showDialogAnimation, 16, 200, Easing.Linear, (d, f) =>
            {
                bg.IsVisible = false;
                dialog.IsVisible = false;
                dialog.TranslationY = PopUpBgLayout.Height;

                OnDialogClosed(new EventArgs());
            });

            OnDialogClosing(new EventArgs());
        }

        private static Animation TransLateYAnimation(VisualElement element, double from, double to)
        {
            return new Animation(d => { element.TranslationY = d; }, from, to);
        }

        private static Animation TransLateXAnimation(VisualElement element, double from, double to)
        {
            return new Animation(d => { element.TranslationX = d; }, from, to);
        }

        private static Animation OpacityAnimation(VisualElement element, double from, double to)
        {
            return new Animation(d => { element.Opacity = d; }, from, to);
        }


    }
}
