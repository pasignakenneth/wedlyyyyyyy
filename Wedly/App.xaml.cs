using System;
using Wedly.Views;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace Wedly
{
    public partial class App : Application
    {
        public static double ScreenWidth 
        { 
            get; set; 
        }
        public static double ScreenHeight 
        { 
            get; set; 
        }
        public static float DeviceScale 
        { 
            get; set; 
        }
        public static double ScreenScale
        {
            get
            {
                float tempWidth = 0;
                float tempHeight = 0;

                if (ScreenWidth < 320.0f) 
                { 
                    tempWidth = (float)ScreenWidth; 
                }
                else 
                { 
                    tempWidth = 320.0f; 
                }

                if (ScreenHeight < 568.0f) 
                { 
                    tempHeight = (float)ScreenHeight; 
                }
                else 
                { 
                    tempHeight = 568.0f; 
                }

                return (ScreenWidth + ScreenHeight) / (320.0f + tempHeight);
            }
        }

        public App()
        {
            InitializeComponent();

            if (Application.Current.Properties.ContainsKey("user_active"))
                Application.Current.MainPage = new NavigationPage(new DashboardPage());
            else
                Application.Current.MainPage = new NavigationPage(new MainPage());
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
