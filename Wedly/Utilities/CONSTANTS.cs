﻿using System;
using System.Collections.ObjectModel;
using Wedly.Models;
using Wedly.Utilities.DependencyServices;
using Xamarin.Forms;

namespace Wedly.Utilities
{
    public class CONSTANTS
    {
        public static readonly ObservableCollection<Users> getUsers = new ObservableCollection<Users>();
        public static readonly ObservableCollection<Users> getInvitedUsers = new ObservableCollection<Users>();
        public static readonly ObservableCollection<Plans> getPlansOwn = new ObservableCollection<Plans>();
        public static readonly ObservableCollection<Plans> getPlansInvited = new ObservableCollection<Plans>();
        public static readonly ObservableCollection<Attendees> getAttendees = new ObservableCollection<Attendees>();
        public static readonly ObservableCollection<Attendees> getInvitedAttendees = new ObservableCollection<Attendees>();
        public static readonly ObservableCollection<Comments> getComments = new ObservableCollection<Comments>();
        public static readonly ObservableCollection<InviteUsers> inviteUsers = new ObservableCollection<InviteUsers>();
        public static Pagination getPagination = new Pagination();
        public static Users getCurrentUser = new Users();
        public static Users dashboardInfo = new Users();

        public static string ROOT_URL = "http://192.168.1.77:3000";
        public static string URL = "http://192.168.1.77:3000/api";
        public static string URL_VERSION = "/v1";
        public static string URL_REGISTER = "/register";
        public static string URL_SIGNIN = "/sign-in";
        public static string URL_PLANS = "/plans";
        public static string URL_USERS = "/users";
        public static string URL_PROFILE = "/profile";
        public static string URL_TEST_ID_TOKEN = "/58?token=6218170f255e335ad89530684c251da2da45737b";
        public static string URL_TEST_DASHBOARD = "?token=";

        public static void ShortMessage(string message)
        {
            DependencyService.Get<iToastMessage>().ShortAlert(message);
        }
        public static void LongMessage(string message)
        {
            DependencyService.Get<iToastMessage>().LongAlert(message);
        }
    }
}
