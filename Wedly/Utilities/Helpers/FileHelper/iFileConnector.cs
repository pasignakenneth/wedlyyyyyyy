﻿using System;
using System.Threading;
using Newtonsoft.Json.Linq;

namespace Wedly.Utilities.Helpers.FileHelper
{
    public interface iFileConnector
    {
        void ReceiveJSONDataAsync(JObject jsonData, CancellationToken ct);
        void ReceiveTimeoutError(int error, string errorStr);
    }
}
