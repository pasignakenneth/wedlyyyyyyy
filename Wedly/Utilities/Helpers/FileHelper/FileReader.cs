﻿using System;
using System.IO;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Wedly.Utilities.Helpers.FileHelper
{
    public class FileReader : iFileReader
    {
        public static FileReader fileReader;
        public static FileReader GetInstance
        {
            get
            {
                if (fileReader == null)
                    fileReader = new FileReader();
                return fileReader;
            }
        }
        WeakReference<iFileConnector> _fileReaderDelegate;
        public iFileConnector FileReaderDelegate
        {
            get
            {
                iFileConnector fileReaderDelegate;
                return _fileReaderDelegate.TryGetTarget(out fileReaderDelegate) ? fileReaderDelegate : null;
            }
            set
            {
                _fileReaderDelegate = new WeakReference<iFileConnector>(value);
            }
        }

        public async Task ReadFile(string fileName, CancellationToken ct)
        {
            var assembly = typeof(FileReader).GetTypeInfo().Assembly;
            Stream stream = assembly.GetManifestResourceStream("Wedly.Utilities.Files." + fileName);
            using(var reader = new StreamReader(stream))
            {
                var json = reader.ReadToEnd();
                FileReaderDelegate?.ReceiveJSONDataAsync(JObject.Parse(json), ct);
            }
        }


    }
}
