﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace Wedly.Utilities.Helpers.FileHelper
{
    public interface iFileReader
    {
        Task ReadFile(string fileName, CancellationToken ct);
    }
}
