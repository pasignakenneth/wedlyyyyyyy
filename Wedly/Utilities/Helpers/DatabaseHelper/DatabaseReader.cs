﻿using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SQLite;

namespace Wedly.Utilities.Helpers.DatabaseHelper
{
    public class DatabaseReader : iDatabaseReader
    {
        readonly SQLiteAsyncConnection database;

        public static DatabaseReader databaseReader;
        public static DatabaseReader GetInstance
        {
            get
            {
                if (databaseReader == null)
                    databaseReader = new DatabaseReader(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "TodoSQLite.db3"));
                return databaseReader;
            }
        }
        WeakReference<iDatabaseConnector> _databaseReaderDelegate;
        public iDatabaseConnector DatabaseReaderDelegate
        {
            get
            {
                iDatabaseConnector databaseReaderDelegate;
                return _databaseReaderDelegate.TryGetTarget(out databaseReaderDelegate) ? databaseReaderDelegate : null;
            }
        }

        public DatabaseReader(string databasePath)
        {
            database = new SQLiteAsyncConnection(databasePath);
        }

        public void CreateTable<TableName>() where TableName : new()
        {
            database.CreateTableAsync<TableName>().Wait();
        }

        public async Task FetchData<TableName>(CancellationToken ct) where TableName : new()
        {
            await database.CreateTableAsync<TableName>();
            var list = await database.Table<TableName>().ToListAsync();
            var parseData = JsonConvert.SerializeObject(new { list });
            DatabaseReaderDelegate?.ReceiveJSONDataAsync(JObject.Parse(parseData), ct);
        }

        public async Task SaveData<TableName>(JObject jsonData, string tableName) where TableName : new()
        {
            var obj = JObject.Parse(JsonConvert.SerializeObject(new { jsonData }));
            var data = database.QueryAsync<TableName>("SELECT * FROM [" + tableName + "] WHERE [id] = " + obj["jsonData"]["id"]);
            if (data.Result.Count == 0)
                await database.InsertAsync(jsonData);
        }
    }
}
