﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace Wedly.Utilities.Helpers.DatabaseHelper
{
    public interface iDatabaseReader
    {
        void CreateTable<TableName>() where TableName : new();
        Task FetchData<TableName>(CancellationToken ct) where TableName: new();
        Task SaveData<TableName>(JObject jsonData, string tableName) where TableName : new();
    }
}
