﻿using System;
using System.Threading;
using Newtonsoft.Json.Linq;

namespace Wedly.Utilities.Helpers.DatabaseHelper
{
    public interface iDatabaseConnector
    {
        void ReceiveJSONDataAsync(JObject jsonData, CancellationToken ct);
    }
}
