﻿using System;
using System.Threading.Tasks;
using Plugin.Connectivity;

namespace Wedly.Utilities.Helpers.NetworkReader
{
    public class NetworkReader : iNetworkReader
    {
        public static NetworkReader network;
        public static NetworkReader GetInstance
        {
            get
            {
                if (network == null)
                    network = new NetworkReader();
                return network;
            }
        }
        
        public bool HasInternet()
        {
            if (!CrossConnectivity.IsSupported)
                return true;
            return CrossConnectivity.Current.IsConnected;
        }

        public async Task<bool> IsHostReachable()
        {
            if (!CrossConnectivity.Current.IsConnected)
                return false;
            
            return true;
        }
    }
}
