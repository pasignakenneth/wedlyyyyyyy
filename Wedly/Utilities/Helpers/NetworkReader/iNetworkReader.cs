﻿using System;
using System.Threading.Tasks;

namespace Wedly.Utilities.Helpers.NetworkReader
{
    public interface iNetworkReader
    {
        bool HasInternet();
        Task<bool> IsHostReachable();
    }
}
