﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Wedly.Utilities.Helpers.WebServiceReader
{
    public class WebServiceReader : iWebServiceReader
    {
        HttpClient client;
        NetworkReader.NetworkReader networkReader = NetworkReader.NetworkReader.GetInstance;

        WeakReference<iWebServiceConnector> _webServiceReaderDelegate;
        public iWebServiceConnector WebServiceReaderDelegate
        {
            get
            {
                iWebServiceConnector webServiceConnector;
                return _webServiceReaderDelegate.TryGetTarget(out webServiceConnector) ? webServiceConnector : null;
            }
            set
            {
                _webServiceReaderDelegate = new WeakReference<iWebServiceConnector>(value);
            }
        }
        public WebServiceReader()
        {
            client = new HttpClient();
            client.MaxResponseContentBufferSize = 256000;
        }

        public async Task CreateData(string url, object sampleModel, CancellationToken ct)
        {
            var uri = new Uri(string.Format(url));
            var json = JsonConvert.SerializeObject(sampleModel);
            var content = new StringContent(json, Encoding.UTF8, "application/json");

            try
            {
                HttpResponseMessage responseMessage = await client.PostAsync(uri, content, ct);

                if (responseMessage.IsSuccessStatusCode)
                {
                    System.Diagnostics.Debug.WriteLine("Responding...");
                    var data = await responseMessage.Content.ReadAsStringAsync().ConfigureAwait(false);
                    WebServiceReaderDelegate?.ReceiveJSONDataAsync(JObject.Parse(data), ct);
                }
                else
                    System.Diagnostics.Debug.WriteLine("No response");
            }
            catch (Exception e) 
            { 
                System.Diagnostics.Debug.WriteLine("Server is busy\n"+e.StackTrace); 
            }
        }
        public async Task RetrieveData(string url, CancellationToken ct)
        {
            if (networkReader.HasInternet())
            {
                if (await networkReader.IsHostReachable())
                {
                    try
                    {
                        var uri = new Uri(string.Format(url, string.Empty));
                        var responseMessage = await client.GetAsync(uri, ct);
                       

                        if (responseMessage.IsSuccessStatusCode)
                        {
                            ct.ThrowIfCancellationRequested();
                            var content = await responseMessage.Content.ReadAsStringAsync().ConfigureAwait(false);
                            ct.ThrowIfCancellationRequested();
                            WebServiceReaderDelegate?.ReceiveJSONDataAsync(JObject.Parse(content), ct);
                        }
                        else
                        {
                            System.Diagnostics.Debug.WriteLine("No response");
                        }
                    }
                    catch (Exception ex)
                    {
                        System.Diagnostics.Debug.WriteLine(ex.StackTrace);
                        WebServiceReaderDelegate?.ReceiveTimeoutError(1, "Failed");
                    }
                }
            }
            else
            {
                WebServiceReaderDelegate?.ReceiveTimeoutError(1, "Failed");
            }
        }

        public async Task UpdataData(string url, object sampleModel, CancellationToken ct)
        {
            var uri = new Uri(string.Format(url));
            var json = JsonConvert.SerializeObject(sampleModel);
            var content = new StringContent(json, Encoding.UTF8, "application/json");

            HttpResponseMessage responseMessage = await client.PutAsync(uri, content, ct);

            if(responseMessage.IsSuccessStatusCode)
            {
                System.Diagnostics.Debug.WriteLine("Successful update. Responding...");
                var data = await responseMessage.Content.ReadAsStringAsync().ConfigureAwait(false);
                WebServiceReaderDelegate?.ReceiveJSONDataAsync(JObject.Parse(data), ct);
            }
            else
            {
                System.Diagnostics.Debug.WriteLine("No response");
            }
        }
        public async  Task DeleteData(int id, CancellationToken ct)
        {
            //var uri = new Uri(string.Format(Constants.URL + Constants.URL_VERSION + Constants.URL_SIGNIN + "/" + id, string.Empty));

            //HttpResponseMessage responseMessage = await client.DeleteAsync(uri, ct);

            //if(responseMessage.IsSuccessStatusCode)
            //{
            //    System.Diagnostics.Debug.WriteLine("Deleted"+id);
            //}
        }

        public async Task MultiPartDataContentAsync(string url, List<string> arrayKeys, CancellationToken ct, object item = null, Stream[] image = null, List<string> extraKeys = null)
        {
            if (networkReader.HasInternet())
            {
                if (await networkReader.IsHostReachable())
                {
                    var dictionary = JsonConvert.SerializeObject(item);
                    var uri = new Uri(url);
                    //var tokenJson = JObject.Parse(dictionary)["token"];
                    var multipartFormData = new MultipartFormDataContent();

                    int x = 0;

                    foreach (var xkey in arrayKeys)
                    {
                        var json = JObject.Parse(dictionary)[xkey];
                        foreach (var obj in json)
                        {
                            string[] keys = obj.Path.Split('.');
                            try
                            {
                                if (keys.Length < 2)
                                {
                                    keys[0] = keys[0].Remove(keys[0].IndexOf('[') + 1) + "]";
                                    if (!string.IsNullOrEmpty(obj.First.ToString()))
                                    {
                                        foreach (var child in obj)
                                        {
                                            string[] childKeys = child.Path.Split('.');
                                            childKeys[0] = childKeys[0].Remove(childKeys[0].IndexOf('[') + 1) + "]";
                                            if (childKeys[1] == "id" || childKeys[1] == "role")
                                            {
                                                string name;
                                                if (childKeys[1] == "id")
                                                {
                                                    name = "\"" + childKeys[0] + "[user_id]" + "\"";
                                                }
                                                else
                                                {
                                                    name = "\"" + childKeys[0] + "[" + childKeys[1] + "]" + "\"";
                                                }
                                                StringContent content = new StringContent(child.First.ToString(), System.Text.Encoding.UTF8);
                                                content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("form-data") { Name = name };
                                                multipartFormData.Add(content);
                                            }
                                        }
                                    }
                                }
                                else if ( keys[1].Contains("_photo") && image[x] != null )
                                {
                                    string name = "\"" + keys[0] + "[" + keys[1] + "]" + "\"";
                                    //StreamContent content = new StreamContent(file.GetStream());
                                    StreamContent content = new StreamContent(image[x]);
                                    x++;
                                    content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("form-data") { FileName = "\"" + keys[1] + ".jpeg\"", Name = name };
                                    multipartFormData.Add(content);
                                }
                                else
                                {
                                    if (!string.IsNullOrEmpty(obj.First.ToString()))
                                    {
                                        string keyName = "\"" + keys[0] + "[" + keys[1] + "]" + "\"";
                                        StringContent content = new StringContent(obj.First.ToString(), System.Text.Encoding.UTF8);
                                        content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("form-data") { Name = keyName };
                                        multipartFormData.Add(content);
                                    }
                                }
                            }
                            catch (Exception e) { }
                        }
                    }


                    HttpResponseMessage response = await client.PostAsync(uri, multipartFormData);
                    if (response.IsSuccessStatusCode)
                    {
                        Debug.WriteLine("Responding...");
                        var result = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                        WebServiceReaderDelegate?.ReceiveJSONDataAsync(JObject.Parse(result), ct);
                    }
                    else
                    {
                        Debug.WriteLine("No response");
                    }
                }
                else
                {
                    WebServiceReaderDelegate?.ReceiveTimeoutError(2, "The URL host for Idle cannot be reached and seems to be unavailable. Please try again later!");
                }
            }
            else
            {
                WebServiceReaderDelegate?.ReceiveTimeoutError(3, "Please check your internet connection, and try again!");
            }
        }
    }
}
