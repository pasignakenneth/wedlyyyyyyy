﻿using System;
using System.Threading;
using Newtonsoft.Json.Linq;

namespace Wedly.Utilities.Helpers.WebServiceReader
{
    public interface iWebServiceConnector
    {
        void ReceiveJSONDataAsync(JObject jsonData, CancellationToken ct);
        void ReceiveTimeoutError(int error, string errorStr);
    }
}
