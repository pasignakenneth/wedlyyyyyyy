﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace Wedly.Utilities.Helpers.WebServiceReader
{
    public interface iWebServiceReader
    {
        Task CreateData(string url, object sampleModel, CancellationToken ct);
        Task RetrieveData(string url, CancellationToken ct);
        Task UpdataData(string url, object sampleModel, CancellationToken ct);
        Task DeleteData(int id, CancellationToken ct);

        Task MultiPartDataContentAsync(string url, List<string> arrayKeys, CancellationToken ct, object item = null, System.IO.Stream[] image = null, List<string> extraKeys = null);
    }
}
