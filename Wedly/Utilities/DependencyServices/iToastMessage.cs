﻿using System;
namespace Wedly.Utilities.DependencyServices
{
    public interface iToastMessage
    {
        void LongAlert(string message);
        void ShortAlert(string message);
    }
}
