﻿using System;
using System.IO;
using System.Threading.Tasks;

namespace Wedly.Utilities.DependencyServices
{
    public interface iImagePicker
    {
        Task<Stream> GetImageFromGalleryAsync();
        Task<string> GetImagePathAsync();
    }
}
