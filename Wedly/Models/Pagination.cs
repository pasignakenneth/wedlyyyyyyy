﻿using System;
namespace Wedly.Models
{
    public class Pagination
    {
        public int plans
        {
            get; set;
        }
        public int off_set
        {
            get; set;
        }
        public int load_more
        {
            get; set;
        }
        public string url
        {
            get; set;
        }
    }
}
