﻿using System;
using System.Collections.ObjectModel;
using SQLite;

namespace Wedly.Models
{
    public class Users
    {
        public int id
        {
            get; set;
        }
        public string email
        {
            get; set;
        }
        public string name
        {
            get; set;
        }
        public string image
        {
            get; set;
        }
        public string token
        {
            get; set;
        }
        public string role
        {
            get; set;
        }
        public ObservableCollection<Plans> plans_created
        {
            get; set;
        }
        public ObservableCollection<Plans> plans_invited
        {
            get; set;
        }
    }
}
