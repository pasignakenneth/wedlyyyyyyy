﻿using System;
using System.Collections.ObjectModel;
using SQLite;

namespace Wedly.Models
{
    public class Plans
    {
        [PrimaryKey]
        public int id
        {
            get;
        }
        public string title
        {
            get; set;
        }
        public string start_at
        {
            get; set;
        }
        public string start_date_at
        {
            get; set;
        }
        public string start_time_at
        {
            get; set;
        }
        public string location
        {
            get; set;
        }
        public string bride_photo
        {
            get; set;
        }
        public string groom_photo
        {
            get; set;
        }
        public string banner_photo
        {
            get; set;
        }
        public Users creator
        {
            get; set;
        }
        public string description
        {
            get; set;
        }
        public int attendees_count
        {
            get; set;
        }
        public int rsvp
        {
            get; set;
        }
        public ObservableCollection<Users> attendees
        {
            get; set;
        }
        public ObservableCollection<Comments> comments
        {
            get; set;
        }
        public string officiator
        {
            get; set;
        }
        public string reception
        {
            get; set;
        }
    }
}
