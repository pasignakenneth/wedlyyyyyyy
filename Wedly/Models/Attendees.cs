﻿using System;
using SQLite;

namespace Wedly.Models
{
    public class Attendees
    {
        [PrimaryKey, AutoIncrement]
        public int id
        {
            get;
        }
        public string name
        {
            get; set;
        }
        public Users users
        {
            get; set;
        }
        public string role
        {
            get; set;
        }
        public int attendance_plan
        {
            get; set;
        }
        public string profile_image
        {
            get; set;
        }
    }
}
