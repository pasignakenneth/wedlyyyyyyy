﻿using System;
using SQLite;

namespace Wedly.Models
{
    public class Comments
    {
        [PrimaryKey, AutoIncrement]
        public int id
        {
            get;
        }
        public string name
        {
            get; set;
        }
        public string comment
        {
            get; set;
        }
        public string image_comment
        {
            get; set;
        }
        public string profile_image
        {
            get; set;
        }
    }
}
