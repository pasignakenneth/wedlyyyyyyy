﻿using System;
using SQLite;

namespace Wedly.Models
{
    public class InviteUsers
    {
        [PrimaryKey]
        public int id
        {
            get; set;
        }

        public int user_id
        {
            get; set;
        }
        public string role
        {
            get; set;
        }
    }
}
