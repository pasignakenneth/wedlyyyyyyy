﻿using System;
using Android.Widget;
using Wedly.Droid.DependencyServices;
using Wedly.Utilities.DependencyServices;
using Xamarin.Forms;

[assembly: Dependency(typeof(ToastMessageService))]
namespace Wedly.Droid.DependencyServices
{
    public class ToastMessageService : iToastMessage
    {
        public void LongAlert(string message)
        {
            Toast.MakeText(Android.App.Application.Context, message, ToastLength.Long).Show();
        }

        public void ShortAlert(string message)
        {
            Toast.MakeText(Android.App.Application.Context, message, ToastLength.Short).Show();
        }
    }
}
