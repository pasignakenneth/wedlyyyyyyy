﻿using System;
using System.IO;
using System.Threading.Tasks;
using Android;
using Android.Content;
using Android.Content.PM;
using Android.Support.V4.App;
using Android.Support.V4.Content;
using Wedly.Droid.DependencyServices;
using Wedly.Utilities.DependencyServices;
using Xamarin.Forms;

[assembly: Dependency(typeof(ImagePickerService))]
namespace Wedly.Droid.DependencyServices
{
	public class ImagePickerService : iImagePicker
    {
        public Task<Stream> GetImageFromGalleryAsync()
        {
            Intent intent = new Intent();
            intent.SetType("image/*");
            intent.SetAction(Intent.ActionGetContent);

            MainActivity.GetInstance.StartActivityForResult(Intent.CreateChooser(intent, "Get Image:"), MainActivity.PickImageId);
            MainActivity.GetInstance.TaskCompletionSource = new TaskCompletionSource<Stream>();
            return MainActivity.GetInstance.TaskCompletionSource.Task;

        }

        public async Task<string> GetImagePathAsync()
        {
            return "te";
        }
    }
}
