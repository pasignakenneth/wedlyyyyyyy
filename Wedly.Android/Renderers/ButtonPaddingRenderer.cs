﻿using System;
using Android.Content;
using Wedly.Droid.Renderers;
using Wedly.Utilities.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(ButtonPaddingRender), typeof(ButtonPaddingRenderer))]
namespace Wedly.Droid.Renderers
{
    public class ButtonPaddingRenderer : ButtonRenderer
    {
        public ButtonPaddingRenderer(Context context) : base(context)
        {
        }

        protected override void OnElementChanged(ElementChangedEventArgs<Button> e)
        {
            base.OnElementChanged(e);
            Control.SetAllCaps(false);
            Control.SetPadding(0, 0, 0, 0);

        }
    }
}
