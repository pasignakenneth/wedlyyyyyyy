﻿using System;
using Android.Content;
using Wedly.Droid.Renderers;
using Wedly.Utilities.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(EntryBorderRender), typeof(EntryBorderRenderer))]
namespace Wedly.Droid.Renderers
{
    public class EntryBorderRenderer : EntryRenderer
    {
        public EntryBorderRenderer(Context context) : base(context)
        {
        }
        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);
            if(e.OldElement == null)
            {
                Control.Background = null;
            }
        }
    }
}
