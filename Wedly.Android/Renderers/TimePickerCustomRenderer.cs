﻿using System;
using Android.Content;
using Wedly.Droid.Renderers;
using Wedly.Utilities.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(TimePickerCustomRender), typeof(TimePickerCustomRenderer))]
namespace Wedly.Droid.Renderers
{
    public class TimePickerCustomRenderer : TimePickerRenderer
    {
        public TimePickerCustomRenderer(Context context) : base(context)
        {
        }

        protected override void OnElementChanged(ElementChangedEventArgs<TimePicker> e)
        {
            base.OnElementChanged(e);

            if(e.OldElement == null)
            {
                Control.Background = null;
                var layoutParams = new MarginLayoutParams(Control.LayoutParameters);
                layoutParams.SetMargins(0, 0, 0, 0);
                LayoutParameters = layoutParams;
                Control.LayoutParameters = layoutParams;
                Control.SetPadding(0, 0, 0, 0);
                SetPadding(0, 0, 0, 0);
                Control.SetTextSize(Android.Util.ComplexUnitType.Px, 13);
            }
        }
    }
}
