﻿using System;

using Android.App;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using System.Threading.Tasks;
using System.IO;
using Android.Content;
using Android.Graphics;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using Plugin.CurrentActivity;
using Android.Provider;
using Android.Database;

namespace Wedly.Droid
{
    [Activity(Label = "Wedly", Icon = "@mipmap/icon", Theme = "@style/MainTheme", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        protected override void OnCreate(Bundle bundle)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            base.OnCreate(bundle);
            var width = Resources.DisplayMetrics.WidthPixels;
            var height = Resources.DisplayMetrics.HeightPixels;
            var density = Resources.DisplayMetrics.Density;

            App.ScreenWidth = width / density;
            App.ScreenHeight = height / density;
            App.DeviceScale = density;

            GetInstance = this;
            CrossCurrentActivity.Current.Init(this, bundle);
            Xamarin.FormsMaps.Init(this, bundle);

            global::Xamarin.Forms.Forms.Init(this, bundle);
            LoadApplication(new App());
        }
        public TaskCompletionSource<Stream> TaskCompletionSource
        {
            get; set;
        }

        public static readonly int PickImageId = 999;
        public static MainActivity GetInstance
        {
            get; private set;
        }
        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);

            if (requestCode == PickImageId)
            {
                if (resultCode == Result.Ok && data != null)
                {
                    Android.Net.Uri uri = data.Data;
                    Stream stream = ContentResolver.OpenInputStream(uri);
                    System.Diagnostics.Debug.WriteLine(uri);

                    string path = GetFilePath(uri);
                    System.Diagnostics.Debug.WriteLine("Path: "+path);


                    var editedImage = ResizeImage(StreamtoByte(stream), 80, 80);
                    //for saving image on phone
                    //BitmapFactory.Options options = new BitmapFactory.Options();
                    //options.InPurgeable = true;
                    //Bitmap originalImage = BitmapFactory.DecodeByteArray(StreamtoByte(stream), 0, StreamtoByte(stream).Length, options);
                    //Bitmap resizedImage = Bitmap.CreateScaledBitmap(originalImage, 1080, 1080, true);
                    //originalImage.Recycle();
                    //var pathUri = GetPathToImage(uri);
                    //using (var os = new System.IO.FileStream(Android.OS.Environment.ExternalStorageDirectory + "/DCIM/Camera/MikeBitMap2.jpg", System.IO.FileMode.CreateNew))
                    //{
                    //    resizedImage.Compress(Bitmap.CompressFormat.Jpeg, 95, os);
                    //}
                    TaskCompletionSource.SetResult(new MemoryStream(editedImage));
                }
                else
                {
                    TaskCompletionSource.SetResult(null);
                }
            }
        }

        private string GetFilePath(Android.Net.Uri uri)
        {
            string doc_id = "";
            using (var c1 = ContentResolver.Query(uri, null, null, null, null))
            {
                c1.MoveToFirst();
                string document_id = c1.GetString(0);
                doc_id = document_id.Substring(document_id.LastIndexOf(":") + 1);
            }

            string path = null;

            if (doc_id.EndsWith(".jpeg") || doc_id.EndsWith(".png") || doc_id.EndsWith(".jpg"))
            {
                return doc_id;
            }

            // The projection contains the columns we want to return in our query.
            string selection = Android.Provider.MediaStore.Images.Media.InterfaceConsts.Id + " =? ";
            using (var cursor = ContentResolver.Query(Android.Provider.MediaStore.Images.Media.ExternalContentUri, null, selection, new string[] { doc_id }, null))
            {
                if (cursor == null) return path;
                var columnIndex = cursor.GetColumnIndexOrThrow(Android.Provider.MediaStore.Images.Media.InterfaceConsts.Data);
                cursor.MoveToFirst();
                path = cursor.GetString(columnIndex);
            }
            return path;


        }


        public byte[] ResizeImage(byte[] imageData, float width, float height)
        {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.InPurgeable = true;
            Bitmap originalImage = BitmapFactory.DecodeByteArray(imageData, 0, imageData.Length, options);
            Bitmap resizedImage = Bitmap.CreateScaledBitmap(originalImage, (int)width, (int)height, true);
            originalImage.Recycle();


            //save image


            using (MemoryStream ms = new MemoryStream())
            {
                resizedImage.Compress(Bitmap.CompressFormat.Jpeg, 100, ms);
                resizedImage.Recycle();

                return ms.ToArray();
            }

        }
        public static void SaveImage(byte[] imageData, int width, int height)
        {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.InPurgeable = true;
            Bitmap originalImage = BitmapFactory.DecodeByteArray(imageData, 0, imageData.Length, options);
            Bitmap resizedImage = Bitmap.CreateScaledBitmap(originalImage, width, height, true);
            originalImage.Recycle();

            try
            {
                using (var os = new FileStream(Android.OS.Environment.ExternalStorageDirectory + "/DCIM/Camera/First.jpg", FileMode.CreateNew))
                {
                    resizedImage.Compress(Bitmap.CompressFormat.Jpeg, 96, os);
                    Console.WriteLine("Saved");
                }
            }
            catch (Exception e)
            {
                Console.Write("Error" + e.StackTrace);
            }
        }
        void ExportBitmapAsPNG(Bitmap bitmap)
        {
            var sdCardPath = Android.OS.Environment.ExternalStorageDirectory.AbsolutePath;
            var filePath = System.IO.Path.Combine(sdCardPath, "test.png");
            var stream = new FileStream(filePath, FileMode.Create);
            bitmap.Compress(Bitmap.CompressFormat.Png, 100, stream);
            stream.Close();

        }



        public static byte[] StreamtoByte(Stream input)
        {
            byte[] buffer = new byte[16 * 1024];
            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }
        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, Android.Content.PM.Permission[] grantResults)
        {
            Plugin.Permissions.PermissionsImplementation.Current.OnRequestPermissionsResult(requestCode, permissions, grantResults);

        }

        public string GetPathToImage(Android.Net.Uri uri)
        {
            string doc_id = "";
            using (var cl = ContentResolver.Query(uri, null, null, null, null))
            {
                cl.MoveToFirst();
                string document_id = cl.GetString(0);
                doc_id = document_id.Substring(document_id.LastIndexOf(":") + 1);
            }

            string path = null;

            string selection = Android.Provider.MediaStore.Images.Media.InterfaceConsts.Id + " =? ";
            using (var cursor = ContentResolver.Query(Android.Provider.MediaStore.Images.Media.ExternalContentUri, null, selection, new string[] { doc_id }, null))
            {
                if (cursor == null)
                    return path;
                var columnIndex = cursor.GetColumnIndexOrThrow(Android.Provider.MediaStore.Images.Media.InterfaceConsts.Data);
                cursor.MoveToFirst();
                path = cursor.GetString(columnIndex);
            }
            return path;

        }
    }
}

